﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class CPUPaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20f;
	public Transform target;


	// Use this for initialization
	void Start () {
		//GameObject ThePuck = GameObject.Find ("Puck");
		//Debug.Log (ThePuck.transform.position);


		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	// Update is called once per frame
	void Update () {
		//KeyboardInput ();
		PuckAI();

	}

	/*void KeyboardInput () {
		GameObject ThePuck = GameObject.Find ("Puck");
		Debug.Log (ThePuck.transform.position);

		Vector3 direction = Vector3.zero;

		//direction.x = Input.GetAxis ("Horizontal");
		//direction.y = Input.GetAxis ("Vertical");

		direction.x = ThePuck.transform.TransformDirection  ;
		direction.y = ThePuck.transform.position.y;



		Vector3 vel = direction.normalized * speed;

		float move = speed * Time.fixedDeltaTime;
		float distToTarget = direction.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;

	}*/

	void PuckAI() {

			// get the vector from the bee to the target 
			// and normalise it.
			Vector3 direction = target.position - transform.position;
			direction = direction.normalized;

			Vector3 velocity = direction * speed;
			//transform.Translate(velocity * Time.deltaTime);
		rigidbody.AddForce(velocity * Time.deltaTime);

	}

}
