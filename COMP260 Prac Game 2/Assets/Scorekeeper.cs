﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text winnerText;

	void Start () {    
		// subscribe to events from all the Goals
		Goal[] goals = FindObjectsOfType<Goal> ();

		for (int i = 0; i < goals.Length; i++) {
			goals [i].scoreGoalEvent += OnScoreGoal;


		}
		// reset the scores to zero
		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";


		}

		winnerText.text = "";

	}

	public void OnScoreGoal(int player) {
		

		// add points to the player whose goal it is
		score[player] += scorePerGoal;
			scoreText[player].text = score[player].ToString ();
		Debug.Log ("Player " + player + ": " + score[player]);

		if(score[player] == 1){
			winnerText.text = ("Player " + player + " is the winner!");
		}

		//if (score[player] = 0) {
			//Debug.Log ("test");
		//}

	}
}
